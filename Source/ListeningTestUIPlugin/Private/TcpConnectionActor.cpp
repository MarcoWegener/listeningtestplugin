// Fill out your copyright notice in the Description page of Project Settings.


#include "TcpConnectionActor.h"
#include "MushraInterfaceActor.h"
#include "HeadMountedDisplayFunctionLibrary.h"

ATcpConnectionActor::ATcpConnectionActor() : IPAddress("127.0.0.1"), PortNumber(4000), ConnectOnStartup(false), ServerMode(0), HeadTrackingSpeed(0.05f)
{

}

void ATcpConnectionActor::BeginPlay()
{
	if (ConnectOnStartup)
	{
		ConnectToServer();
	}

	HeadTrackingPositions.Reserve(1500);
	HeadTrackingRotations.Reserve(1500);
}

void ATcpConnectionActor::OnConnected(int32 ConnectionID)
{
	UE_LOG(LogTemp, Log, TEXT("Connected to server."));
}

void ATcpConnectionActor::OnDisconnected(int32 ConnectionID)
{
	UE_LOG(LogTemp, Log, TEXT("Disconnected from server."));
}

void ATcpConnectionActor::OnMessageReceived(int32 ConnectionID, TArray<uint8>& Message)
{
	UE_LOG(LogTemp, Log, TEXT("Message received."));

	while (Message.Num() != 0)
	{
		uint8 msglength = Message_ReadByte(Message);
		if (msglength < 0)
		{
			return;
		}
		TArray<uint8> MessageContent;


		if (!Message_ReadBytes(msglength, Message, MessageContent))
		{
			continue;
		}

		//process message
		uint8 OpCode;
		FString MessageString;
		AActor* FoundActor;
		AUserInterfaceActor* InterfaceActor;
		switch (ServerMode)
		{
		case 0: //Mushra
			OpCode = (uint8)MessageContent[0];
			MessageContent.RemoveAt(0);
			FoundActor = UGameplayStatics::GetActorOfClass(GetWorld(), AMushraInterfaceActor::StaticClass());
			InterfaceActor = Cast<AMushraInterfaceActor>(FoundActor);
			switch (OpCode)
			{
			case 3: //Startup
				UE_LOG(LogTemp, Log, TEXT("Received startup data."));
				MessageString = ConvBytes2String(MessageContent);
				InterfaceActor->ProcessStartupData(MessageString);
				break;
			case 4:
				break;
			}
			break;
		default:
			UE_LOG(LogTemp, Error, TEXT("Failed to process Server Mode."));
			break;
		}
	}
}

void ATcpConnectionActor::ConnectToServer()
{
	if (isConnected(ConnectionIDServer))
	{
		UE_LOG(LogTemp, Error, TEXT("Cannot connect. Already connected."));
		return;
	}

	FTcpSocketDisconnectDelegate disconnectDelegate;
	disconnectDelegate.BindDynamic(this, &ATcpConnectionActor::OnDisconnected);
	FTcpSocketConnectDelegate connectDelegate;
	connectDelegate.BindDynamic(this, &ATcpConnectionActor::OnConnected);
	FTcpSocketReceivedMessageDelegate receivedDelegate;
	receivedDelegate.BindDynamic(this, &ATcpConnectionActor::OnMessageReceived);
	Connect(IPAddress, PortNumber, disconnectDelegate, connectDelegate, receivedDelegate, ConnectionIDServer);
}

void ATcpConnectionActor::ProcessMessage(TArray<uint8>& Message)
{
	
}

void ATcpConnectionActor::SendMessage(TArray<uint8> Message)
{
	//check connection
	if (!isConnected(ConnectionIDServer))
	{
		UE_LOG(LogTemp, Error, TEXT("Cannot send message. Not connected to a server."));
		return;
	}

	//read current time
	float CurrentTime = GetWorld()->GetTimeSeconds();
	Message.Append(Conv_FloatToBytes(CurrentTime));

	//create header
	int32 messagelength = Message.Num();
	FString messagelengthstring = FString::FromInt(messagelength);
	FString zerofill;
	switch (messagelengthstring.Len())
	{
	case 1:
		zerofill = "000";
		break;
	case 2:
		zerofill = "00";
		break;
	case 3:
		zerofill = "0";
		break;
	case 4:
		zerofill = "";
		break;
	default:
		break;
	}
	FString messageheader = zerofill + messagelengthstring;

	//build message
	TArray<uint8> ProcessedMessage = Conv_StringToBytes(messageheader);
	ProcessedMessage.Append(Message);

	//send data
	UE_LOG(LogTemp, Log, TEXT("Processed message. Sending Data."));
	if (!SendData(ConnectionIDServer, ProcessedMessage))
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to send message."));
	}
}

FString ATcpConnectionActor::ConvBytes2String(TArray<uint8> Bytes)
{
	int32 size = Bytes.Num();
	char *messagestring = new char[size];
	for (int32 i = 0; i < Bytes.Num(); i++)
	{
		messagestring[i] = (char)Bytes[i];
	}
	return FString(messagestring);
}

void ATcpConnectionActor::EnableHeadTracking()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayConnected() && UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ATcpConnectionActor::CollectHeadTrackingData, HeadTrackingSpeed, true, 0.f);
		HeadTrackingEnabled = true;
	}
	else
	{
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ATcpConnectionActor::CollectPlayerPosition, HeadTrackingSpeed, true, 0.f);
		HeadTrackingEnabled = true;
	}
}

void ATcpConnectionActor::DisableHeadTracking()
{
	if (!HeadTrackingEnabled)
	{
		UE_LOG(LogTemp, Warning, TEXT("Head tracking not running."));
		return;
	}

	GetWorldTimerManager().ClearTimer(TimerHandle);
	HeadTrackingEnabled = false;
}

bool ATcpConnectionActor::isHeadTrackingEnabled()
{
	return HeadTrackingEnabled;
}

void ATcpConnectionActor::CollectHeadTrackingData()
{
	FRotator CurrentRotation;
	FVector CurrentOrientation;
	UHeadMountedDisplayFunctionLibrary::GetOrientationAndPosition(CurrentRotation, CurrentOrientation);
	HeadTrackingPositions.Add(CurrentOrientation);
	HeadTrackingRotations.Add(CurrentRotation);

	if (HeadTrackingPositions.Num() >= 1500 || HeadTrackingRotations.Num() >= 1500)
	{
		WriteToFile();
		HeadTrackingPositions.Reset(1500);
		HeadTrackingRotations.Reset(1500);
	}
}

void ATcpConnectionActor::CollectPlayerPosition()
{
	FVector CurrentPosition = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
	HeadTrackingPositions.Add(CurrentPosition);
	HeadTrackingRotations.Add(FRotator({ 0,0,0 }));

	if (HeadTrackingPositions.Num() >= 1500 || HeadTrackingRotations.Num() >= 1500)
	{
		WriteToFile();
		HeadTrackingPositions.Reset(1500);
		HeadTrackingRotations.Reset(1500);
	}
}

void ATcpConnectionActor::WriteToFile()
{
	if (HeadTrackingPositions.Num() == 0 || HeadTrackingRotations.Num() == 0)
	{
		return;
	}

	IPlatformFile& FileManager = FPlatformFileManager::Get().GetPlatformFile();
	TArray<FString> StringData = TArray<FString>();
	StringData.Reserve(1500);

	//Check array sizes
	if (HeadTrackingPositions.Num() != HeadTrackingRotations.Num())
	{
		UE_LOG(LogTemp, Error, TEXT("Arrays must be same size!"));
		return;
	}

	//Combine into single array
	for (int32 i = 0; i < HeadTrackingRotations.Num(); i++)
	{
		StringData.Add(HeadTrackingRotations[i].ToString() + TEXT(" ") + HeadTrackingPositions[i].ToString());
	}

	//write to file
	if (!FileManager.FileExists(*HeadTrackingFileName))
	{
		UE_LOG(LogTemp, Error, TEXT("File does not exist!"));
		return;
	}

	if (FFileHelper::SaveStringArrayToFile(StringData, *HeadTrackingFileName, FFileHelper::EEncodingOptions::AutoDetect, &IFileManager::Get(),
		EFileWrite::FILEWRITE_Append))
	{
		UE_LOG(LogTemp, Log, TEXT("File successfully written."));
		return;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to write file."));
		return;
	}
}