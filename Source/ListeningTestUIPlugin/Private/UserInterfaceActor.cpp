// Fill out your copyright notice in the Description page of Project Settings.


#include "UserInterfaceActor.h"

// Sets default values
AUserInterfaceActor::AUserInterfaceActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AUserInterfaceActor::BeginPlay()
{
	Super::BeginPlay();
	AActor* foundActor = UGameplayStatics::GetActorOfClass(GetWorld(), ATcpConnectionActor::StaticClass());
	if (!foundActor)
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to find Tcp Connection."));
		return;
	}
	TcpConnection = Cast<ATcpConnectionActor>(foundActor);
}

// Called every frame
void AUserInterfaceActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AUserInterfaceActor::ProcessStartupData(FString Message)
{

}

ATcpConnectionActor* AUserInterfaceActor::GetTcpConnection()
{
	return TcpConnection;
}