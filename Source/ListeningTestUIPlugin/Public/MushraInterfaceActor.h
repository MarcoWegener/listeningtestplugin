// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UserInterfaceActor.h"
#include "Components/WidgetComponent.h"
#include "MushraInterfaceActor.generated.h"

/**
 * Actor class for mushra-styled user interface
 */
UCLASS()
class LISTENINGTESTUIPLUGIN_API AMushraInterfaceActor : public AUserInterfaceActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "User Interface") UWidgetComponent* MushraInterfaceWidgetComponent;

protected:
	AMushraInterfaceActor();
	virtual void BeginPlay() override;
	void SetWidgetProperties();

	float SliderAStartValue;
	float SliderBStartValue;
	float SliderCStartValue;
	bool ResetSliders;
	bool DiscreteSliders;
	TArray<FString> Attributes;
	TArray<FString> LevelNames;
	bool RandomizeLevels = 0;

public:
	virtual void ProcessStartupData(FString Message) override;

private:
	UClass* MushraInterfaceWidgetClass;
};
