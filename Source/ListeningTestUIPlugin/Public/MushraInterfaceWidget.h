// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/Slider.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"
#include "MushraInterfaceWidget.generated.h"

/**
 * Mushra-styled user interface. User can switch between different levels and adjust sliders rating attributes.
 * Scores are sent to TCP server.
 */
UCLASS()
class LISTENINGTESTUIPLUGIN_API UMushraInterfaceWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	virtual void NativeConstruct() override;

// ********************
// **** Properties ****
// ********************
	float SliderAStartValue;
	float SliderBStartValue;
	float SliderCStartValue;
	bool ResetSliders;
	bool DiscreteSliders;
	FString TextAttributeContent = TEXT("Attribute");
	TArray<FString> Attributes;
	TArray<FString> LevelNames;
	int32 LevelState = 0;
	int32 CurrentAttribute = 0;

// ***************************
// **** Widget Components ****
// ***************************
public:
	//Buttons
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UButton* ButtonA;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UButton* ButtonB;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UButton* ButtonC;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UButton* ButtonNextAttribute;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UButton* ButtonPrevAttribute;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UButton* ButtonFinish;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UButton* ButtonHelp;

	//Sliders
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) USlider* SliderA;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) USlider* SliderB;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) USlider* SliderC;

	//Text
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UTextBlock* TextAttribute;

// *****************************
// **** Blueprint Functions ****
// *****************************

	UFUNCTION(BlueprintCallable, Category = "Mushra Interface") void UpdateWidget();

// *********************************
// **** Setter/Getter Functions ****
// *********************************
	UFUNCTION(BlueprintCallable, Category = "Mushra") void setStartValues(float A = 0.0, float B = 0.0, float C = 0.0);
	UFUNCTION(BlueprintCallable, Category = "Mushra") void setSliderReset(bool reset = false);
	UFUNCTION(BlueprintCallable, Category = "Mushra") void setSliderDiscrete(bool discrete = false);
	UFUNCTION(BlueprintCallable, Category = "Mushra") void setAttributes(TArray<FString> newattr);
	UFUNCTION(BlueprintCallable, Category = "Mushra") void setLevelNames(TArray<FString> newlvl);
	UFUNCTION(BlueprintCallable, Category = "Mushra") void setLevelState(uint8 lvlst);

	UFUNCTION(BlueprintPure, Category = "Mushra") float getSliderAStartValue();
	UFUNCTION(BlueprintPure, Category = "Mushra") float getSliderBStartValue();
	UFUNCTION(BlueprintPure, Category = "Mushra") float getSliderCStartValue();
	UFUNCTION(BlueprintPure, Category = "Mushra") bool getSliderReset();
	UFUNCTION(BlueprintPure, Category = "Mushra") bool getSliderDiscrete();

	UFUNCTION(BlueprintCallable, Category = "User Interface|Levels") void ChangeSublevel(FString LevelName);
	UFUNCTION(BlueprintCallable, Category = "User Interface|Levels") void UpdateSublevel();


// *************************
// **** Widget Callback ****
// *************************
	UFUNCTION() void ButtonAFeedback();
	UFUNCTION() void ButtonBFeedback();
	UFUNCTION() void ButtonCFeedback();
	UFUNCTION() void ButtonPrevFeedback();
	UFUNCTION() void ButtonNextFeedback();
	UFUNCTION() void ButtonFinishFeedback();
	UFUNCTION() void ButtonHelpFeedback();
	UFUNCTION() void SliderAFeedback();
	UFUNCTION() void SliderBFeedback();
	UFUNCTION() void SliderCFeedback();
};