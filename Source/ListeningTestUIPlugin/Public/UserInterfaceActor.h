// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TcpConnectionActor.h"
#include "UserInterfaceActor.generated.h"

/**
* Parent class for all user interface actors. user interfaces are attached to these actors to be placed in world 
*/
UCLASS()
class LISTENINGTESTUIPLUGIN_API AUserInterfaceActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUserInterfaceActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool HeadTrackingEnabled = 0;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void ProcessStartupData(FString Message);
	ATcpConnectionActor* GetTcpConnection();

protected:
	ATcpConnectionActor* TcpConnection;

};
